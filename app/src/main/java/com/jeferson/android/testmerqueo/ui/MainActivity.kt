package com.jeferson.android.testmerqueo.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jeferson.android.testmerqueo.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}