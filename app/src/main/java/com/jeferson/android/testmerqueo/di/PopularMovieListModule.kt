package com.jeferson.android.testmerqueo.di

import com.jeferson.android.testmerqueo.presentation.PopularMovieListViewModel
import com.jeferson.android.testmerqueo.usecases.*
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class PopularMovieListModule {

    @Provides
    fun popularMovieListViewModelProvider(
        getPopularMoviesUseCase: GetPopularMoviesUseCase,
        getMoviesLocalDBUseCase: GetMoviesLocalDBUseCase,
        getAddAllPopularMoviesUseCase: AddAllPopularMoviesUseCase,
        getAllShoppingCartMoviesUseCase: GetAllShoppingCartMoviesUseCase,
        updateShoppingCartMovieStatusUseCase: UpdateShoppingCartMovieStatusUseCase
    ) = PopularMovieListViewModel(
        getPopularMoviesUseCase,
        getMoviesLocalDBUseCase,
        getAddAllPopularMoviesUseCase,
        getAllShoppingCartMoviesUseCase,
        updateShoppingCartMovieStatusUseCase
    )
}

@Subcomponent(modules = [(PopularMovieListModule::class)])
interface PopularMovieListComponent {
    val popularMovieListViewModel: PopularMovieListViewModel
}