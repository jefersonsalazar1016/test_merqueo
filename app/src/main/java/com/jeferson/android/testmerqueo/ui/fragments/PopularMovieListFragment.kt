package com.jeferson.android.testmerqueo.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jeferson.android.testmerqueo.R
import com.jeferson.android.testmerqueo.adapters.MovieGridAdapter
import com.jeferson.android.testmerqueo.databinding.FragmentPopularMovieListBinding
import com.jeferson.android.testmerqueo.di.PopularMovieListComponent
import com.jeferson.android.testmerqueo.di.PopularMovieListModule
import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.domain.ShoppingCartMovieDomain
import com.jeferson.android.testmerqueo.presentation.PopularMovieListViewModel
import com.jeferson.android.testmerqueo.presentation.utils.Event
import com.jeferson.android.testmerqueo.ui.OnItemMovieClickListener
import com.jeferson.android.testmerqueo.ui.OnShoppingCartMovieClickListener
import com.jeferson.android.testmerqueo.utils.*

class PopularMovieListFragment : Fragment(), OnItemMovieClickListener,
    OnShoppingCartMovieClickListener {

    private lateinit var binding: FragmentPopularMovieListBinding

    private val messageErrorFactory = MessageErrorFactory()
    private val globalCartMovieList: MutableList<ShoppingCartMovieDomain> = mutableListOf()

    // adapter for recyclerview
    private lateinit var movieGridAdapter: MovieGridAdapter

    // dagger component
    private lateinit var popularMovieListComponent: PopularMovieListComponent

    private val popularMovieListViewModel: PopularMovieListViewModel by lazy {
        getViewModel {
            popularMovieListComponent.popularMovieListViewModel
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // initialize dagger component
        popularMovieListComponent = requireContext().app.component.inject(PopularMovieListModule())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate<FragmentPopularMovieListBinding>(
            inflater,
            R.layout.fragment_popular_movie_list,
            container,
            false
        ).apply {
            lifecycleOwner = this@PopularMovieListFragment
            viewModel = popularMovieListViewModel
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Connect adapters
        movieGridAdapter = MovieGridAdapter(
            this,
            this
        ).apply {
            setHasStableIds(true)
        }

        // initialize recyclerview list
        binding.rvPopularMovieList.run {
            adapter = movieGridAdapter
            addOnScrollListener(onScrollListener)
        }

        // observe events
        popularMovieListViewModel.events.observe(viewLifecycleOwner, Observer(this::validateEvents))

        // validate network state
        if (ActivityUtils.isNetworkAvailable(requireContext())) {
            // call api rest
            popularMovieListViewModel.onGetPopularMovies()
        } else {
            // get data from room
            popularMovieListViewModel.getMoviesLocalDB()

            // show network error
            val dialog = messageErrorFactory.getDialog(
                requireContext(),
                MessageErrorFactory.NETWORK_ERROR
            )
            dialog.show()
        }

        binding.tvRetryGetMovies.setOnClickListener {
            // call api rest
            popularMovieListViewModel.onGetPopularMovies()
        }

        // add swipe refresh event
        binding.swipeRefreshMovieList.setOnRefreshListener {
            popularMovieListViewModel.onRetryGetPopularMovies()
        }

        binding.frameShoppingCart.setOnClickListener {
            findNavController().navigate(R.id.navShoppingCartMovieListFragment)
        }

        // shopping cart section
        popularMovieListViewModel.shoppingCartMoviesList.observe(
            viewLifecycleOwner,
            Observer(popularMovieListViewModel::onChangeSizeShoppingCartMoviesList)
        )
    }

    // listener for load more movies
    private val onScrollListener: RecyclerView.OnScrollListener by lazy {
        object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val layoutManager = recyclerView.layoutManager as GridLayoutManager
                val visibleItemCount: Int = layoutManager.childCount
                val totalItemCount: Int = layoutManager.itemCount
                val firstVisibleItemPosition: Int = layoutManager.findFirstVisibleItemPosition()

                // validate network state
                if (ActivityUtils.isNetworkAvailable(context!!)) {
                    popularMovieListViewModel.onLoadMoreItems(
                        visibleItemCount,
                        firstVisibleItemPosition,
                        totalItemCount
                    )
                }
            }
        }
    }

    // handle livedata events
    private fun validateEvents(event: Event<PopularMovieListViewModel.PopularMovieListNavigation>?) {
        event?.getContentIfNotHandled()?.let { navigation ->
            when (navigation) {
                is PopularMovieListViewModel.PopularMovieListNavigation.ShowMovieError -> navigation.run {
                    val dialog = messageErrorFactory.getDialog(requireContext(), error)
                    dialog.show()

                    // get data from room
                    popularMovieListViewModel.getMoviesLocalDB()
                }
                is PopularMovieListViewModel.PopularMovieListNavigation.ShowPopularMovieList -> navigation.run {
                    movieGridAdapter.setDataList(popularMovieList)
                    movieGridAdapter.updateStatusMovies(globalCartMovieList)
                }
                is PopularMovieListViewModel.PopularMovieListNavigation.SaveDataPopularMovieList -> navigation.run {
                    popularMovieListViewModel.saveMovies(popularMovieList)
                }
                is PopularMovieListViewModel.PopularMovieListNavigation.ShowShoppingCartMoviesList -> navigation.run {
                    globalCartMovieList.clear()
                    globalCartMovieList.addAll(cartMoviesList)

                    movieGridAdapter.updateStatusMovies(cartMoviesList)
                }
                PopularMovieListViewModel.PopularMovieListNavigation.HideLoading -> {
                    binding.swipeRefreshMovieList.isRefreshing = false
                }
                PopularMovieListViewModel.PopularMovieListNavigation.ShowLoading -> {
                    binding.swipeRefreshMovieList.isRefreshing = true
                }
            }
        }
    }


    // event for go to the detail fragment
    override fun openMovieDetail(movie: ResultDomain) {
        val bundle = bundleOf(Constants.ID_MOVIE_DETAIL to movie.id)
        findNavController().navigate(R.id.navMovieDetailFragment, bundle)
    }

    // events to add or remove a movie from the shopping cart
    override fun onUpdateStatusShoppingCartMovie(shoppingCartMovieDomain: ShoppingCartMovieDomain) {
        popularMovieListViewModel.updateShoppingCartMovieStatus(shoppingCartMovieDomain)
    }
}