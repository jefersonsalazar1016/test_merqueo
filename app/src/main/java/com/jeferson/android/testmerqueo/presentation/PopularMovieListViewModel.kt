package com.jeferson.android.testmerqueo.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.domain.ShoppingCartMovieDomain
import com.jeferson.android.testmerqueo.presentation.utils.Event
import com.jeferson.android.testmerqueo.usecases.*
import com.jeferson.android.testmerqueo.utils.MessageErrorFactory.Companion.GENERIC_ERROR
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException

class PopularMovieListViewModel(
    private val getPopularMoviesUseCase: GetPopularMoviesUseCase,
    private val getMoviesLocalDBUseCase: GetMoviesLocalDBUseCase,
    private val getAddAllPopularMoviesUseCase: AddAllPopularMoviesUseCase,
    private val getAllShoppingCartMoviesUseCase: GetAllShoppingCartMoviesUseCase,
    private val updateShoppingCartMovieStatusUseCase: UpdateShoppingCartMovieStatusUseCase
) : ViewModel() {

    private val disposable = CompositeDisposable()
    private val _popularMovieList: MutableList<ResultDomain> = mutableListOf()

    // Live data events
    private val _events = MutableLiveData<Event<PopularMovieListNavigation>>()
    val events: LiveData<Event<PopularMovieListNavigation>> get() = _events

    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean> get() = _isLoading

    private val _sizeMoviesList = MutableLiveData(0)
    val sizeMoviesList: LiveData<Int> get() = _sizeMoviesList

    // get shopping cart movies
    val shoppingCartMoviesList: LiveData<List<ShoppingCartMovieDomain>>
        get() = LiveDataReactiveStreams.fromPublisher(getAllShoppingCartMoviesUseCase.invoke())

    private val _isAddedToShoppingCart = MutableLiveData<Boolean>()
    val isAddedToShoppingCart: LiveData<Boolean> get() = _isAddedToShoppingCart

    private val _sizeShoppingCartMoviesList = MutableLiveData(0)
    val sizeShoppingCartMoviesList: LiveData<Int> get() = _sizeShoppingCartMoviesList

    // properties for requests
    private var currentPage = 1
    private var isLastPage = false


    // recyclerview list scroll listener
    fun onLoadMoreItems(visibleItemCount: Int, firstVisibleItemPosition: Int, totalItemCount: Int) {
        if (isLoading.value!! || isLastPage || !isInFooter(
                visibleItemCount,
                firstVisibleItemPosition,
                totalItemCount
            )
        ) {
            return
        }

        currentPage += 1
        onGetPopularMovies()
    }

    fun onRetryGetPopularMovies() {
        _popularMovieList.clear()
        currentPage = 1
        isLastPage = false
        onGetPopularMovies()
    }

    // call for get movies in Api REST
    fun onGetPopularMovies() {
        disposable.add(
            getPopularMoviesUseCase
                .invoke(currentPage)
                .doOnSubscribe { showLoading() }
                .subscribe({ popularMovieList ->
                    _popularMovieList.addAll(popularMovieList)
                    _sizeMoviesList.value = _popularMovieList.size
                    hideLoading()

                    if (popularMovieList.size < PAGE_SIZE) {
                        isLastPage = true
                    }

                    // send movie list to view with live data event
                    _events.value = Event(PopularMovieListNavigation.ShowPopularMovieList(_popularMovieList))

                    // save movies from the first three pages
                    if (currentPage <= 3) {
                        _events.value = Event(
                            PopularMovieListNavigation.SaveDataPopularMovieList(popularMovieList)
                        )
                    }
                }, { error ->
                    isLastPage = true
                    hideLoading()

                    var errorCode: Int = GENERIC_ERROR

                    try {
                        val httpError = error as HttpException
                        errorCode = httpError.code()
                    } catch (e: Exception) {
                        e.message?.let { Log.e("ERROR GET MOVIES", it) }
                    }

                    _events.value = Event(PopularMovieListNavigation.ShowMovieError(errorCode))
                })
        )
    }

    // save the first three response from the API
    fun saveMovies(movieList: List<ResultDomain>) {
        disposable.add(
            getAddAllPopularMoviesUseCase.invoke(movieList)
                .subscribe {
                    Log.d("INSERT MOVIES", "SUCCESS")
                }
        )
    }

    // get movie list from local db
    fun getMoviesLocalDB() {
        disposable.add(
            getMoviesLocalDBUseCase.invoke()
                .doOnSubscribe { showLoading() }
                .subscribe { resultList ->
                    hideLoading()

                    // Show only the db result when global list is empty
                    if (_popularMovieList.size == 0) {
                        // send movie list to view with live data event
                        _sizeMoviesList.value = resultList.size
                        _events.value = Event(PopularMovieListNavigation.ShowPopularMovieList(resultList))
                    } else {
                        // send movie list to view with live data event
                        _sizeMoviesList.value = _popularMovieList.size
                        _events.value = Event(PopularMovieListNavigation.ShowPopularMovieList(_popularMovieList))
                    }
                }
        )
    }

    private fun isInFooter(
        visibleItemCount: Int,
        firstVisibleItemPosition: Int,
        totalItemCount: Int
    ): Boolean {
        return visibleItemCount + firstVisibleItemPosition >= totalItemCount
                && firstVisibleItemPosition >= 0
                && totalItemCount >= PAGE_SIZE
    }


    // handling progress
    private fun showLoading() {
        _isLoading.value = true
        _events.value = Event(PopularMovieListNavigation.ShowLoading)
    }

    private fun hideLoading() {
        _isLoading.value = false
        _events.value = Event(PopularMovieListNavigation.HideLoading)
    }


    // clear disposable
    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    // events for handling navigation in view
    sealed class PopularMovieListNavigation {
        data class ShowMovieError(val error: Int) : PopularMovieListNavigation()
        data class ShowPopularMovieList(val popularMovieList: List<ResultDomain>)
            : PopularMovieListNavigation()
        data class SaveDataPopularMovieList(val popularMovieList: List<ResultDomain>)
            : PopularMovieListNavigation()
        data class ShowShoppingCartMoviesList(val cartMoviesList: List<ShoppingCartMovieDomain>)
            : PopularMovieListNavigation()

        object HideLoading : PopularMovieListNavigation()
        object ShowLoading : PopularMovieListNavigation()
    }

    companion object {
        private const val PAGE_SIZE = 20
    }


    // shopping cart section
    fun onChangeSizeShoppingCartMoviesList(shoppingList: List<ShoppingCartMovieDomain>) {
        _sizeShoppingCartMoviesList.value = shoppingList.size

        _events.value = Event(PopularMovieListNavigation.ShowShoppingCartMoviesList(shoppingList))
    }

    fun updateShoppingCartMovieStatus(shoppingCartMovieDomain: ShoppingCartMovieDomain) {
        disposable.add(
            updateShoppingCartMovieStatusUseCase
                .invoke(shoppingCartMovieDomain)
                .subscribe { isAddedToShoppingCart ->
                    _isAddedToShoppingCart.value = isAddedToShoppingCart
                }
        )
    }
}