package com.jeferson.android.testmerqueo.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jeferson.android.testmerqueo.domain.ShoppingCartMovieDomain
import com.jeferson.android.testmerqueo.presentation.utils.Event
import com.jeferson.android.testmerqueo.usecases.DeleteAllShoppingCartMoviesUseCase
import com.jeferson.android.testmerqueo.usecases.GetAllShoppingCartMoviesUseCase
import io.reactivex.disposables.CompositeDisposable

class ShoppingCartMoviesViewModel(
    private val getAllShoppingCartMoviesUseCase: GetAllShoppingCartMoviesUseCase,
    private val deleteAllShoppingCartMoviesUseCase: DeleteAllShoppingCartMoviesUseCase
) : ViewModel() {

    private val disposable = CompositeDisposable()

    // Live data events
    private val _events = MutableLiveData<Event<ShoppingCartMoviesListNavigation>>()
    val events: LiveData<Event<ShoppingCartMoviesListNavigation>> get() = _events

    val shoppingCartMoviesList: LiveData<List<ShoppingCartMovieDomain>>
        get() = LiveDataReactiveStreams.fromPublisher(getAllShoppingCartMoviesUseCase.invoke())

    private val _sizeShoppingCartMoviesList = MutableLiveData(0)
    val sizeShoppingCartMoviesList: LiveData<Int> get() = _sizeShoppingCartMoviesList

    fun onGetShoppingCartMoviesList(shoppingList: List<ShoppingCartMovieDomain>) {
        _sizeShoppingCartMoviesList.value = shoppingList.size

        _events.value =
            Event(ShoppingCartMoviesListNavigation.ShowShoppingCartMoviesList(shoppingList))
    }

    fun onDeleteAllShoppingCartMovies() {
        disposable.add(
            deleteAllShoppingCartMoviesUseCase
                .invoke()
                .subscribe {
                    Log.d("DELETE ALL MOVIES", "SUCCESS")
                }
        )
    }

    // events for handling navigation in view
    sealed class ShoppingCartMoviesListNavigation {
        data class ShowShoppingCartMoviesList(val cartMoviesList: List<ShoppingCartMovieDomain>) :
            ShoppingCartMoviesListNavigation()
    }

    // clear disposable
    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}