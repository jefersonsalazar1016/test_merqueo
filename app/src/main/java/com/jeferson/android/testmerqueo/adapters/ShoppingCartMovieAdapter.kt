package com.jeferson.android.testmerqueo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeferson.android.testmerqueo.R
import com.jeferson.android.testmerqueo.databinding.ItemShoppingCartMovieBinding
import com.jeferson.android.testmerqueo.domain.ShoppingCartMovieDomain
import com.jeferson.android.testmerqueo.imagemanager.bindImageUrl
import com.jeferson.android.testmerqueo.requestmanager.APIConstants

class ShoppingCartMovieAdapter :
    RecyclerView.Adapter<ShoppingCartMovieAdapter.ShoppingCartMovieViewHolder>() {

    private val shoppingCartMoviesList: MutableList<ShoppingCartMovieDomain> = mutableListOf()

    fun setDataShoppingCart(cartMoviesList: List<ShoppingCartMovieDomain>) {
        shoppingCartMoviesList.clear()
        shoppingCartMoviesList.addAll(cartMoviesList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingCartMovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_shopping_cart_movie, parent, false)
        return ShoppingCartMovieViewHolder(view)
    }

    override fun onBindViewHolder(holder: ShoppingCartMovieViewHolder, position: Int) {
        holder.bind(shoppingCartMoviesList[position])
    }

    override fun getItemId(position: Int): Long = shoppingCartMoviesList[position].id.toLong()

    override fun getItemCount(): Int = shoppingCartMoviesList.size

    class ShoppingCartMovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemShoppingCartMovieBinding.bind(view)

        fun bind(shoppingCartMovie: ShoppingCartMovieDomain) {
            binding.movie = shoppingCartMovie

            binding.imageShoppingCartMovie.bindImageUrl(
                url = APIConstants.BASE_IMAGE_W500_URL + shoppingCartMovie.poster_path,
                placeholder = R.drawable.ic_camera,
                errorPlaceholder = R.drawable.ic_broken_image
            )
        }
    }
}