package com.jeferson.android.testmerqueo.parcelables

import com.jeferson.android.testmerqueo.domain.ResultDomain

fun ResultDomain.toResultParcelable() = ResultParcelable(
    adult,
    backdrop_path,
    genre_ids,
    id,
    original_language,
    original_title,
    overview,
    popularity,
    poster_path,
    release_date,
    title,
    video,
    vote_average,
    vote_count
)

fun ResultParcelable.toResultDomain() = ResultDomain(
    adult,
    backdrop_path,
    genre_ids,
    id,
    original_language,
    original_title,
    overview,
    popularity,
    poster_path,
    release_date,
    title,
    video,
    vote_average,
    vote_count
)