package com.jeferson.android.testmerqueo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeferson.android.testmerqueo.R
import com.jeferson.android.testmerqueo.databinding.ItemReviewMovieBinding
import com.jeferson.android.testmerqueo.domain.ReviewResultDomain

class ReviewsMovieAdapter : RecyclerView.Adapter<ReviewsMovieAdapter.ReviewsViewHolder>() {

    private val reviewsMovieList: MutableList<ReviewResultDomain> = mutableListOf()

    fun setDataList(reviewsList: List<ReviewResultDomain>) {
        reviewsMovieList.clear()
        reviewsMovieList.addAll(reviewsList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_review_movie, parent, false)
        return ReviewsViewHolder(view)
    }

    override fun getItemCount(): Int = reviewsMovieList.size

    override fun getItemId(position: Int): Long = reviewsMovieList[position].id.toLong()

    override fun onBindViewHolder(holder: ReviewsViewHolder, position: Int) {
        holder.bind(reviewsMovieList[position])
    }

    class ReviewsViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        private val binding = ItemReviewMovieBinding.bind(view)

        fun bind(review: ReviewResultDomain) {
            binding.review = review
        }
    }

}