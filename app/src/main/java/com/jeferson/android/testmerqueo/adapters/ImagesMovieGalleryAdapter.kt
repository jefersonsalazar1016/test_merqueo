package com.jeferson.android.testmerqueo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeferson.android.testmerqueo.R
import com.jeferson.android.testmerqueo.databinding.ItemImagesMovieBinding
import com.jeferson.android.testmerqueo.domain.BackdropDomain
import com.jeferson.android.testmerqueo.imagemanager.bindImageUrl
import com.jeferson.android.testmerqueo.requestmanager.APIConstants.BASE_IMAGE_W500_URL

class ImagesMovieGalleryAdapter :
    RecyclerView.Adapter<ImagesMovieGalleryAdapter.ImagesMovieViewHolder>() {

    private val imagesList: MutableList<BackdropDomain> = mutableListOf()

    fun setDataImages(list: List<BackdropDomain>) {
        imagesList.clear()
        imagesList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImagesMovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_images_movie, parent, false)
        return ImagesMovieViewHolder(view)
    }

    override fun getItemCount(): Int = imagesList.size

    override fun onBindViewHolder(holder: ImagesMovieViewHolder, position: Int) {
        holder.bind(imagesList[position])
    }

    class ImagesMovieViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        private val binding = ItemImagesMovieBinding.bind(view)

        fun bind(item: BackdropDomain) {
            binding.imageBackdropMovie.bindImageUrl(
                url = BASE_IMAGE_W500_URL + item.file_path,
                placeholder = R.drawable.ic_camera,
                errorPlaceholder = R.drawable.ic_broken_image
            )
        }
    }
}