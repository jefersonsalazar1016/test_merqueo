package com.jeferson.android.testmerqueo.ui

import com.jeferson.android.testmerqueo.domain.ResultDomain

interface OnItemMovieClickListener {
    fun openMovieDetail(movie: ResultDomain)
}