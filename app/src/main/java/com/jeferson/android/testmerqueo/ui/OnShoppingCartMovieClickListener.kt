package com.jeferson.android.testmerqueo.ui

import com.jeferson.android.testmerqueo.domain.ShoppingCartMovieDomain

interface OnShoppingCartMovieClickListener {
    fun onUpdateStatusShoppingCartMovie(shoppingCartMovieDomain: ShoppingCartMovieDomain)
}