package com.jeferson.android.testmerqueo

import android.app.Application
import com.jeferson.android.testmerqueo.di.DaggerTestMerqueoComponent
import com.jeferson.android.testmerqueo.di.TestMerqueoComponent

class TestMerqueoApp : Application() {

    lateinit var component: TestMerqueoComponent

    override fun onCreate() {
        super.onCreate()
        component = initAppComponent()
    }

    private fun initAppComponent() = DaggerTestMerqueoComponent.factory().create(this)
}