package com.jeferson.android.testmerqueo.ui.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.appbar.AppBarLayout
import com.jeferson.android.testmerqueo.R
import com.jeferson.android.testmerqueo.adapters.ImagesMovieGalleryAdapter
import com.jeferson.android.testmerqueo.adapters.ReviewsMovieAdapter
import com.jeferson.android.testmerqueo.databinding.FragmentMovieDetailBinding
import com.jeferson.android.testmerqueo.di.DetailMovieComponent
import com.jeferson.android.testmerqueo.di.DetailMovieModule
import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.imagemanager.bindImageUrl
import com.jeferson.android.testmerqueo.presentation.DetailMovieViewModel
import com.jeferson.android.testmerqueo.presentation.utils.Event
import com.jeferson.android.testmerqueo.requestmanager.APIConstants
import com.jeferson.android.testmerqueo.utils.Constants
import com.jeferson.android.testmerqueo.utils.MessageErrorFactory
import com.jeferson.android.testmerqueo.utils.app
import com.jeferson.android.testmerqueo.utils.getViewModel

class MovieDetailFragment : Fragment() {

    private val messageErrorFactory = MessageErrorFactory()

    // adapter for recyclerview
    private val imagesMovieGalleryAdapter by lazy { ImagesMovieGalleryAdapter() }
    private val reviewsMovieAdapter by lazy { ReviewsMovieAdapter() }

    // properties for show or hide toolbar title
    private var isToolbarShow = true
    private var scrollRange = -1

    private lateinit var binding: FragmentMovieDetailBinding

    // dagger component
    private lateinit var detailMovieComponent: DetailMovieComponent

    private val movieDetailViewModel: DetailMovieViewModel by lazy {
        getViewModel {
            detailMovieComponent.detailMovieViewModel
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // initialize dagger component
        detailMovieComponent = requireContext().app.component.inject(
            DetailMovieModule()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentMovieDetailBinding>(
            inflater,
            R.layout.fragment_movie_detail,
            container,
            false
        ).apply {
            lifecycleOwner = this@MovieDetailFragment
            viewModel = movieDetailViewModel
        }

        binding.toolbarDetailMovie.navigationIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_arrow_back_white)

        binding.toolbarDetailMovie.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListenerScrollToolbar()

        // initialize recyclerview list
        binding.rvImagesMovieList.run {
            adapter = imagesMovieGalleryAdapter
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        }
        binding.rvReviewsMovieList.run {
            adapter = reviewsMovieAdapter
        }

        // observe data and events
        movieDetailViewModel.movieValues.observe(
            viewLifecycleOwner,
            Observer(this::loadMovieValues)
        )
        movieDetailViewModel.events.observe(viewLifecycleOwner, Observer(this::validateEvents))
        movieDetailViewModel.getMovieById(arguments?.getInt(Constants.ID_MOVIE_DETAIL)!!)


        // Add or remove movie to the cart events
        binding.fabAddToCartMovie.setOnClickListener {
            updateStatusShoppingCartMovie()
        }

        binding.fabRemoveToCartMovie.setOnClickListener {
            updateStatusShoppingCartMovie()
        }

        // event for retry get images
        binding.tvRetryDetailMovie.setOnClickListener {
            movieDetailViewModel.getMovieById(arguments?.getInt(Constants.ID_MOVIE_DETAIL)!!)
        }
    }

    // Called when the user taps the add or remove button
    private fun updateStatusShoppingCartMovie() {
        movieDetailViewModel.onUpdateShoppingCartMovieStatus()
    }

    private fun setListenerScrollToolbar() {
        binding.appBarDetailMovie.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { barLayout, verticalOffset ->
            if (scrollRange == -1) {
                scrollRange = barLayout?.totalScrollRange!!
            }

            if (scrollRange + verticalOffset == 0) {
                binding.collapsingToolbarDetail.setCollapsedTitleTextColor(Color.rgb(255, 255, 255))
                binding.collapsingToolbarDetail.title = getString(R.string.title_detail_movie)

                isToolbarShow = true
            } else if (isToolbarShow) {
                binding.collapsingToolbarDetail.title = " " // space between double-quote is required
                isToolbarShow = false
            }
        })
    }

    // set data in layout with data binding
    private fun loadMovieValues(movie: ResultDomain) {
        binding.imageDetailMovie.bindImageUrl(
            url = APIConstants.BASE_IMAGE_ORIGINAL_URL + movie.backdrop_path,
            placeholder = R.drawable.ic_camera,
            errorPlaceholder = R.drawable.ic_broken_image
        )

        binding.movieDataTitle = movie.title
        binding.movieDataOverview = movie.overview
        binding.movieDataReleaseDate = movie.release_date
    }

    // handle livedata events
    private fun validateEvents(event: Event<DetailMovieViewModel.MovieDetailNavigation>?) {
        event?.getContentIfNotHandled()?.let { navigation ->
            when (navigation) {
                DetailMovieViewModel.MovieDetailNavigation.CloseActivity -> {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.message_not_movie),
                        Toast.LENGTH_LONG
                    ).show()
                    findNavController().navigateUp()
                }
                DetailMovieViewModel.MovieDetailNavigation.HideImagesMovieLoading -> {
                    binding.loadingImagesMovie.visibility = View.INVISIBLE
                }
                DetailMovieViewModel.MovieDetailNavigation.ShowImagesMovieLoading -> {
                    binding.loadingImagesMovie.visibility = View.VISIBLE
                }
                is DetailMovieViewModel.MovieDetailNavigation.ShowImagesMovieError -> navigation.run {
                    val dialog = messageErrorFactory.getDialog(requireContext(), error)
                    dialog.show()
                }
                is DetailMovieViewModel.MovieDetailNavigation.ShowImagesMovieList -> navigation.run {
                    imagesMovieGalleryAdapter.setDataImages(imagesMovieList)
                }
                is DetailMovieViewModel.MovieDetailNavigation.ShowReviewsMovieList -> navigation.run {
                    reviewsMovieAdapter.setDataList(reviewsMovieList)
                }
            }
        }
    }
}