package com.jeferson.android.testmerqueo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jeferson.android.testmerqueo.R
import com.jeferson.android.testmerqueo.databinding.ItemGridMovieBinding
import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.domain.ShoppingCartMovieDomain
import com.jeferson.android.testmerqueo.imagemanager.bindImageUrl
import com.jeferson.android.testmerqueo.requestmanager.APIConstants.BASE_IMAGE_W500_URL
import com.jeferson.android.testmerqueo.ui.OnItemMovieClickListener
import com.jeferson.android.testmerqueo.ui.OnShoppingCartMovieClickListener

class MovieGridAdapter(
    var listenerOpenDetail: OnItemMovieClickListener,
    var listenerShoppingCartMovie: OnShoppingCartMovieClickListener
) : RecyclerView.Adapter<MovieGridAdapter.MovieGridViewHolder>() {

    private val popularMovieList: MutableList<ResultDomain> = mutableListOf()

    fun setDataList(movieList: List<ResultDomain>) {
        popularMovieList.clear()
        popularMovieList.addAll(movieList)
        notifyDataSetChanged()
    }

    fun updateStatusMovies(cartMovieList: List<ShoppingCartMovieDomain>) {
        for (movieDomain in popularMovieList) {
            for (shoppingCartMovie in cartMovieList) {
                if (movieDomain.id == shoppingCartMovie.id) {
                    movieDomain.isAddedToShoppingCart = true
                    break
                } else {
                    movieDomain.isAddedToShoppingCart = false
                }
            }
            if (cartMovieList.isEmpty()) {
                movieDomain.isAddedToShoppingCart = false
            }
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieGridViewHolder {
        val inflater = LayoutInflater.from(parent.context);
        val view = inflater.inflate(R.layout.item_grid_movie, parent, false)

        return MovieGridViewHolder(view, listenerOpenDetail, listenerShoppingCartMovie)
    }

    override fun getItemCount() = popularMovieList.size

    override fun getItemId(position: Int): Long = popularMovieList[position].id.toLong()

    override fun onBindViewHolder(holder: MovieGridViewHolder, position: Int) {
        holder.bind(popularMovieList[position])
    }

    class MovieGridViewHolder(
        view: View,
        private val listenerOpenDetail: OnItemMovieClickListener,
        private val listenerShoppingCartMovie: OnShoppingCartMovieClickListener
    ) : RecyclerView.ViewHolder(view) {

        private val binding = ItemGridMovieBinding.bind(view)

        fun bind(item: ResultDomain) {
            if (item.isAddedToShoppingCart) {
                binding.btnAddMovieMain.visibility = View.GONE
                binding.btnRemoveMovieMain.visibility = View.VISIBLE
            } else {
                binding.btnAddMovieMain.visibility = View.VISIBLE
                binding.btnRemoveMovieMain.visibility = View.GONE
            }

            binding.movie = item

            binding.movieImage.bindImageUrl(
                url = BASE_IMAGE_W500_URL + item.poster_path,
                placeholder = R.drawable.ic_camera,
                errorPlaceholder = R.drawable.ic_broken_image
            )

            itemView.setOnClickListener { listenerOpenDetail.openMovieDetail(item) }


            // change ResultDomain to type ShoppingCartMovieDomain
            val shoppingCartMovieDomain = ShoppingCartMovieDomain(
                item.backdrop_path,
                item.id,
                item.overview,
                item.poster_path,
                item.release_date,
                item.title
            )

            // events to add or remove a movie from the shopping cart
            binding.btnAddMovieMain.setOnClickListener {
                listenerShoppingCartMovie.onUpdateStatusShoppingCartMovie(shoppingCartMovieDomain)
            }
            binding.btnRemoveMovieMain.setOnClickListener {
                listenerShoppingCartMovie.onUpdateStatusShoppingCartMovie(shoppingCartMovieDomain)
            }
        }
    }
}