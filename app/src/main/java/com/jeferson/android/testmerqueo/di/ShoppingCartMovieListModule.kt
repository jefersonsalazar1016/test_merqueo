package com.jeferson.android.testmerqueo.di

import com.jeferson.android.testmerqueo.presentation.ShoppingCartMoviesViewModel
import com.jeferson.android.testmerqueo.usecases.DeleteAllShoppingCartMoviesUseCase
import com.jeferson.android.testmerqueo.usecases.GetAllShoppingCartMoviesUseCase
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class ShoppingCartMovieListModule {

    @Provides
    fun shoppingCartMoviesViewModelProvider(
        getAllShoppingCartMoviesUseCase: GetAllShoppingCartMoviesUseCase,
        deleteAllShoppingCartMoviesUseCase: DeleteAllShoppingCartMoviesUseCase
    ) = ShoppingCartMoviesViewModel(
        getAllShoppingCartMoviesUseCase,
        deleteAllShoppingCartMoviesUseCase
    )
}

@Subcomponent(modules = [(ShoppingCartMovieListModule::class)])
interface ShoppingCartMovieListComponent {
    val shoppingCartMoviesViewModel: ShoppingCartMoviesViewModel
}