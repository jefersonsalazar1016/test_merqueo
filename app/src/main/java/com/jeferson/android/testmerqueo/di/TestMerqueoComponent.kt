package com.jeferson.android.testmerqueo.di

import android.app.Application
import com.jeferson.android.testmerqueo.data.di.RepositoryModule
import com.jeferson.android.testmerqueo.databasemanager.di.DatabaseModule
import com.jeferson.android.testmerqueo.requestmanager.di.APIModule
import com.jeferson.android.testmerqueo.usecases.di.UseCaseModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        APIModule::class,
        DatabaseModule::class,
        UseCaseModule::class,
        RepositoryModule::class
    ]
)
interface TestMerqueoComponent {

    fun inject(module: PopularMovieListModule): PopularMovieListComponent
    fun inject(module: DetailMovieModule): DetailMovieComponent
    fun inject(module: ShoppingCartMovieListModule): ShoppingCartMovieListComponent

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance app: Application): TestMerqueoComponent
    }
}