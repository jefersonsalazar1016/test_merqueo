package com.jeferson.android.testmerqueo.utils

import android.app.AlertDialog
import android.content.Context
import android.util.Log
import com.jeferson.android.testmerqueo.R

class MessageErrorFactory {
    companion object {
        const val GENERIC_ERROR = 0
        const val NETWORK_ERROR = 1
        const val BAD_REQUEST = 400
        const val UNAUTHORIZED = 401
        const val FORBIDDEN = 403
        const val NOT_FOUND = 404
        const val METHOD_NOT_ALLOWED = 405
        const val SERVER_ERROR = 500
        const val INVALID_SERVICE = 501
        const val SERVICE_OFFLINE = 503
    }

    fun getDialog(context: Context, type: Int?): AlertDialog.Builder {
        return when (type) {
            NETWORK_ERROR -> {
                AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.upps))
                    .setMessage(context.getString(R.string.text_network_error))
            }
            BAD_REQUEST -> {
                AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.upps))
                    .setMessage(context.getString(R.string.text_bad_request_error))
            }
            UNAUTHORIZED -> {
                AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.upps))
                    .setMessage(context.getString(R.string.text_unauthorized_error))
            }
            FORBIDDEN -> {
                AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.upps))
                    .setMessage(context.getString(R.string.text_generic_error))
            }
            NOT_FOUND -> {
                AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.upps))
                    .setMessage(context.getString(R.string.text_not_found_error))
            }
            METHOD_NOT_ALLOWED -> {
                AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.upps))
                    .setMessage(context.getString(R.string.text_not_allowed_error))
            }
            SERVER_ERROR -> {
                AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.upps))
                    .setMessage(context.getString(R.string.text_server_error))
            }
            INVALID_SERVICE -> {
                AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.upps))
                    .setMessage(context.getString(R.string.text_invalid_service))
            }
            SERVICE_OFFLINE -> {
                AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.upps))
                    .setMessage(context.getString(R.string.text_service_offline))
            }
            else -> {
                Log.e("ERROR FACTORY", "Se debe agregar el mensaje de error para el código $type")

                AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.upps))
                    .setMessage(context.getString(R.string.text_generic_error))
            }

        }
    }
}