package com.jeferson.android.testmerqueo.di

import com.jeferson.android.testmerqueo.presentation.DetailMovieViewModel
import com.jeferson.android.testmerqueo.usecases.*
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class DetailMovieModule {

    @Provides
    fun detailMovieViewModelProvider(
        getImagesForMovieIdUseCase: GetImagesForMovieIdUseCase,
        getReviewsMovieUseCase: GetReviewsMovieUseCase,
        getShoppingCartMovieStatusUseCase: GetShoppingCartMovieStatusUseCase,
        updateShoppingCartMovieStatusUseCase: UpdateShoppingCartMovieStatusUseCase,
        getMovieByIdUseCase: GetMovieByIdUseCase
    ) = DetailMovieViewModel(
        getImagesForMovieIdUseCase,
        getReviewsMovieUseCase,
        getShoppingCartMovieStatusUseCase,
        updateShoppingCartMovieStatusUseCase,
        getMovieByIdUseCase
    )
}

@Subcomponent(modules = [(DetailMovieModule::class)])
interface DetailMovieComponent {
    val detailMovieViewModel: DetailMovieViewModel
}