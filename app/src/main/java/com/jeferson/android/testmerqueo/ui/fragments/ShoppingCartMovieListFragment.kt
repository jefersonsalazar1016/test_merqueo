package com.jeferson.android.testmerqueo.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.jeferson.android.testmerqueo.R
import com.jeferson.android.testmerqueo.adapters.ShoppingCartMovieAdapter
import com.jeferson.android.testmerqueo.databinding.BottomSheetDialogEmptyCartViewBinding
import com.jeferson.android.testmerqueo.databinding.FragmentShoppingCartMovieListBinding
import com.jeferson.android.testmerqueo.di.ShoppingCartMovieListComponent
import com.jeferson.android.testmerqueo.di.ShoppingCartMovieListModule
import com.jeferson.android.testmerqueo.presentation.ShoppingCartMoviesViewModel
import com.jeferson.android.testmerqueo.presentation.utils.Event
import com.jeferson.android.testmerqueo.utils.app
import com.jeferson.android.testmerqueo.utils.getViewModel

class ShoppingCartMovieListFragment : Fragment() {

    private lateinit var binding: FragmentShoppingCartMovieListBinding

    // adapter for recyclerview
    private val shoppingCartMovieAdapter = ShoppingCartMovieAdapter()

    // dagger component
    private lateinit var shoppingCartMovieListComponent: ShoppingCartMovieListComponent

    private val shoppingCartMoviesViewModel: ShoppingCartMoviesViewModel by lazy {
        getViewModel {
            shoppingCartMovieListComponent.shoppingCartMoviesViewModel
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // initialize dagger component
        shoppingCartMovieListComponent = requireContext().app.component.inject(ShoppingCartMovieListModule())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentShoppingCartMovieListBinding>(
            inflater,
            R.layout.fragment_shopping_cart_movie_list,
            container,
            false
        ).apply {
            lifecycleOwner = this@ShoppingCartMovieListFragment
            viewModel = shoppingCartMoviesViewModel
        }

        binding.toolbarShoppingCart.navigationIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_arrow_back_white)
        binding.toolbarShoppingCart.title = getString(R.string.text_cart)

        binding.toolbarShoppingCart.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // initialize recyclerview list
        binding.rvShoppingCartMoviesList.run {
            adapter = shoppingCartMovieAdapter
        }

        // initialize an instance of divider item decoration
        DividerItemDecoration(
            requireContext(),
            LinearLayoutManager.VERTICAL
        ).apply {
            // add divider item decoration to recycler view
            binding.rvShoppingCartMoviesList.addItemDecoration(this)
        }


        // observe events
        shoppingCartMoviesViewModel.shoppingCartMoviesList.observe(
            viewLifecycleOwner,
            Observer(shoppingCartMoviesViewModel::onGetShoppingCartMoviesList)
        )
        shoppingCartMoviesViewModel.events.observe(
            viewLifecycleOwner,
            Observer(this::validateEvents)
        )


        // remove all movies from the shopping cart
        binding.btnEmptyCart.setOnClickListener { showBottomSheetDialogEmptyCart() }
    }


    // configure bottom sheet dialog
    private fun showBottomSheetDialogEmptyCart() {
        val sheetDialogView = layoutInflater.inflate(R.layout.bottom_sheet_dialog_empty_cart_view, null)

        val bindingSheetDialog = BottomSheetDialogEmptyCartViewBinding.inflate(
            layoutInflater,
            sheetDialogView as ViewGroup,
            false
        )

        val bottomSheetDialog = BottomSheetDialog(requireContext())
        bottomSheetDialog.setContentView(bindingSheetDialog.root)

        val behavior = bottomSheetDialog.behavior
        behavior.state = BottomSheetBehavior.STATE_EXPANDED

        // add onclick events
        bindingSheetDialog.btnCancelEmptyCart.setOnClickListener { bottomSheetDialog.dismiss() }
        bindingSheetDialog.btnConfirmEmptyCart.setOnClickListener {
            shoppingCartMoviesViewModel.onDeleteAllShoppingCartMovies()
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()
    }

    // handle livedata events
    private fun validateEvents(event: Event<ShoppingCartMoviesViewModel.ShoppingCartMoviesListNavigation>?) {
        event?.getContentIfNotHandled()?.let { navigation ->
            when (navigation) {
                is ShoppingCartMoviesViewModel.ShoppingCartMoviesListNavigation.ShowShoppingCartMoviesList -> navigation.run {
                    shoppingCartMovieAdapter.setDataShoppingCart(cartMoviesList)
                }
            }
        }
    }
}