package com.jeferson.android.testmerqueo.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jeferson.android.testmerqueo.domain.BackdropDomain
import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.domain.ReviewResultDomain
import com.jeferson.android.testmerqueo.domain.ShoppingCartMovieDomain
import com.jeferson.android.testmerqueo.presentation.utils.Event
import com.jeferson.android.testmerqueo.usecases.*
import com.jeferson.android.testmerqueo.utils.MessageErrorFactory.Companion.GENERIC_ERROR
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException

class DetailMovieViewModel(
    private val getImagesForMovieIdUseCase: GetImagesForMovieIdUseCase,
    private val getReviewsMovieUseCase: GetReviewsMovieUseCase,
    private val getShoppingCartMovieStatusUseCase: GetShoppingCartMovieStatusUseCase,
    private val updateShoppingCartMovieStatusUseCase: UpdateShoppingCartMovieStatusUseCase,
    private val getMovieByIdUseCase: GetMovieByIdUseCase
) : ViewModel() {

    private var movie: ResultDomain? = null

    private val disposable = CompositeDisposable()
    private val _imagesMovieList: MutableList<BackdropDomain> = mutableListOf()
    private val _reviewsMovieList: MutableList<ReviewResultDomain> = mutableListOf()

    // Live data events
    private val _events = MutableLiveData<Event<MovieDetailNavigation>>()
    val events: LiveData<Event<MovieDetailNavigation>> get() = _events

    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean> get() = _isLoading

    private val _sizeImagesList = MutableLiveData(0)
    val sizeImagesList: LiveData<Int> get() = _sizeImagesList

    private val _sizeReviewsList = MutableLiveData(0)
    val sizeReviewsList: LiveData<Int> get() = _sizeReviewsList

    private val _movieValues = MutableLiveData<ResultDomain>()
    val movieValues: LiveData<ResultDomain> get() = _movieValues

    // live data shopping cart
    private val _isAddedToShoppingCart = MutableLiveData<Boolean>()
    val isAddedToShoppingCart: LiveData<Boolean> get() = _isAddedToShoppingCart


    fun getMovieById(movieId: Int) {
        disposable.add(
            getMovieByIdUseCase.invoke(movieId)
                .subscribe({ resultDomain ->
                    movie = resultDomain
                    initializeMovieLiveData()
                }, {
                    _events.value = Event(MovieDetailNavigation.CloseActivity)
                })
        )
    }

    private fun initializeMovieLiveData() {
        if (movie == null) {
            _events.value = Event(MovieDetailNavigation.CloseActivity)
            return
        }

        _movieValues.value = movie!!

        validateShoppingCartMovieStatus(movie!!.id)
        onGetImagesByMovieId(movie!!.id)
        onGetReviewsMovie(movie!!.id)
    }

    fun onGetImagesByMovieId(movieId: Int) {
        disposable.add(
            getImagesForMovieIdUseCase.invoke(movieId)
                .doOnSubscribe { showLoading() }
                .subscribe({ resultImagesMovieList ->
                    hideLoading()
                    _sizeImagesList.value = resultImagesMovieList.size

                    _imagesMovieList.clear()
                    _imagesMovieList.addAll(resultImagesMovieList)

                    // send images movie list to view with live data event
                    _events.value = Event(MovieDetailNavigation.ShowImagesMovieList(_imagesMovieList))
                }, { error ->
                    hideLoading()
                    _sizeImagesList.value = _imagesMovieList.size

                    var errorCode: Int = GENERIC_ERROR

                    try {
                        val httpError = error as HttpException
                        errorCode = httpError.code()
                    } catch (e: Exception) {
                        e.message?.let { Log.e("ERROR GET IMAGES MOVIE", it) }
                    }

                    _events.value = Event(MovieDetailNavigation.ShowImagesMovieError(errorCode))

                    // Show global list in case of error and if the list is > 0
                    if (_imagesMovieList.size > 0) {
                        _events.value = Event(MovieDetailNavigation.ShowImagesMovieList(_imagesMovieList))
                    }
                })
        )
    }

    private fun onGetReviewsMovie(movieId: Int) {
        disposable.add(
            getReviewsMovieUseCase.invoke(movieId)
                .subscribe({ reviewsList ->
                    _sizeReviewsList.value = reviewsList.size

                    _reviewsMovieList.clear()
                    _reviewsMovieList.addAll(reviewsList)

                    // send reviews movie list to view with live data event
                    _events.value = Event(MovieDetailNavigation.ShowReviewsMovieList(_reviewsMovieList))
                }, { error ->
                    _sizeReviewsList.value = _reviewsMovieList.size
                    Log.e("ERROR GET REVIEWS MOVIE", error?.message ?: "")

                    // Show global list in case of error and if the list is > 0
                    if (_reviewsMovieList.size > 0) {
                        _events.value = Event(MovieDetailNavigation.ShowReviewsMovieList(_reviewsMovieList))
                    }
                })
        )
    }

    // handling progress
    private fun showLoading() {
        _isLoading.value = true
        _events.value = Event(MovieDetailNavigation.ShowImagesMovieLoading)
    }

    private fun hideLoading() {
        _isLoading.value = false
        _events.value = Event(MovieDetailNavigation.HideImagesMovieLoading)
    }


    // shopping cart section
    fun onUpdateShoppingCartMovieStatus() {
        if (movie == null) {
            _events.value = Event(MovieDetailNavigation.CloseActivity)
            return
        }

        // change ResultDomain to type ShoppingCartMovieDomain
        val shoppingCartMovieDomain = ShoppingCartMovieDomain(
            movie!!.backdrop_path,
            movie!!.id,
            movie!!.overview,
            movie!!.poster_path,
            movie!!.release_date,
            movie!!.title
        )

        disposable.add(
            updateShoppingCartMovieStatusUseCase
                .invoke(shoppingCartMovieDomain)
                .subscribe { isAddedToShoppingCart ->
                    _isAddedToShoppingCart.value = isAddedToShoppingCart
                }
        )
    }

    private fun validateShoppingCartMovieStatus(movieId: Int) {
        disposable.add(
            getShoppingCartMovieStatusUseCase
                .invoke(movieId)
                .subscribe { isAddedToShoppingCart ->
                    _isAddedToShoppingCart.value = isAddedToShoppingCart
                }
        )
    }


    // clear disposable
    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }


    // events for handling navigation in view
    sealed class MovieDetailNavigation {
        data class ShowImagesMovieError(val error: Int) : MovieDetailNavigation()
        data class ShowImagesMovieList(val imagesMovieList: List<BackdropDomain>) : MovieDetailNavigation()
        data class ShowReviewsMovieList(val reviewsMovieList: List<ReviewResultDomain>) : MovieDetailNavigation()

        object HideImagesMovieLoading : MovieDetailNavigation()
        object ShowImagesMovieLoading : MovieDetailNavigation()
        object CloseActivity : MovieDetailNavigation()
    }

}