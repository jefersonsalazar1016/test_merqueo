package com.jeferson.android.testmerqueo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.jeferson.android.testmerqueo.domain.BackdropDomain
import com.jeferson.android.testmerqueo.presentation.DetailMovieViewModel
import com.jeferson.android.testmerqueo.presentation.utils.Event
import com.jeferson.android.testmerqueo.usecases.*
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DetailMovieViewModelTest {

    @get:Rule
    val rxSchedulerRules = RxSchedulerRules()

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @Mock
    lateinit var getImagesForMovieIdUseCase: GetImagesForMovieIdUseCase

    @Mock
    lateinit var getReviewsMovieUseCase: GetReviewsMovieUseCase

    @Mock
    lateinit var getShoppingCartMovieStatusUseCase: GetShoppingCartMovieStatusUseCase

    @Mock
    lateinit var updateShoppingCartMovieStatusUseCase: UpdateShoppingCartMovieStatusUseCase

    @Mock
    lateinit var getMovieByIdUseCase: GetMovieByIdUseCase

    @Mock
    lateinit var eventsObserve: Observer<Event<DetailMovieViewModel.MovieDetailNavigation>>

    private lateinit var detailMovieViewModel: DetailMovieViewModel

    @Before
    fun setUp() {
        detailMovieViewModel = DetailMovieViewModel(
            getImagesForMovieIdUseCase,
            getReviewsMovieUseCase,
            getShoppingCartMovieStatusUseCase,
            updateShoppingCartMovieStatusUseCase,
            getMovieByIdUseCase
        )
    }

    @Test
    fun `onGetImagesByMovieId should return an expected success list of backdrops`() {
        val expectedResult = listOf(mockedBackdrop)
        given(getImagesForMovieIdUseCase.invoke(any())).willReturn(Single.just(expectedResult))

        detailMovieViewModel.events.observeForever(eventsObserve)
        detailMovieViewModel.onGetImagesByMovieId(567189)

        verify(eventsObserve).onChanged(
            Event(
                DetailMovieViewModel.MovieDetailNavigation.ShowImagesMovieList(
                    expectedResult
                )
            )
        )
    }
}

val mockedBackdrop = BackdropDomain(
    0.0,
    "",
    100,
    "",
    0.0,
    0,
    100
)