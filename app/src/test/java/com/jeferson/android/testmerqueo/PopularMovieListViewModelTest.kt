package com.jeferson.android.testmerqueo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.presentation.PopularMovieListViewModel
import com.jeferson.android.testmerqueo.presentation.utils.Event
import com.jeferson.android.testmerqueo.usecases.*
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PopularMovieListViewModelTest {

    @get:Rule
    val rxSchedulerRules = RxSchedulerRules()

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @Mock
    lateinit var getPopularMoviesUseCase: GetPopularMoviesUseCase

    @Mock
    lateinit var getMoviesLocalDBUseCase: GetMoviesLocalDBUseCase

    @Mock
    lateinit var getAddAllPopularMoviesUseCase: AddAllPopularMoviesUseCase

    @Mock
    lateinit var getAllShoppingCartMoviesUseCase: GetAllShoppingCartMoviesUseCase

    @Mock
    lateinit var updateShoppingCartMovieStatusUseCase: UpdateShoppingCartMovieStatusUseCase

    @Mock
    lateinit var eventsObserve: Observer<Event<PopularMovieListViewModel.PopularMovieListNavigation>>

    private lateinit var popularMovieListViewModel: PopularMovieListViewModel

    @Before
    fun setUp() {
        popularMovieListViewModel = PopularMovieListViewModel(
            getPopularMoviesUseCase,
            getMoviesLocalDBUseCase,
            getAddAllPopularMoviesUseCase,
            getAllShoppingCartMoviesUseCase,
            updateShoppingCartMovieStatusUseCase
        )
    }

    @Test
    fun `onGetPopularMovies should return an expected success list of movies`() {
        val expectedResult = listOf(mockedResult.copy(id = 460465))
        given(getPopularMoviesUseCase.invoke(any())).willReturn(Single.just(expectedResult))

        popularMovieListViewModel.events.observeForever(eventsObserve)
        popularMovieListViewModel.onGetPopularMovies()

        verify(eventsObserve).onChanged(
            Event(
                PopularMovieListViewModel.PopularMovieListNavigation.ShowPopularMovieList(
                    expectedResult
                )
            )
        )
    }

    @Test
    fun `getMoviesLocalDB should return an expected success list of movies`() {
        val expectedResult = listOf(mockedResult.copy(id = 460465))
        given(getMoviesLocalDBUseCase.invoke()).willReturn(Flowable.just(expectedResult))

        popularMovieListViewModel.events.observeForever(eventsObserve)
        popularMovieListViewModel.getMoviesLocalDB()

        verify(eventsObserve).onChanged(
            Event(
                PopularMovieListViewModel.PopularMovieListNavigation.ShowPopularMovieList(
                    expectedResult
                )
            )
        )
    }
}

val mockedResult = ResultDomain(
    false,
    "",
    null,
    0,
    "",
    "",
    "",
    0.0,
    "",
    "",
    "",
    false,
    0.0f,
    0
)