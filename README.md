# README #

### Requerimientos técnicos para construir el proyecto ###

La aplicación se construyó y compiló usando las siguientes versiones:

* Android Studio 4.2
* Versión del Gradle 4.2.0
* Versión de Kotlin 1.4.31
* SDK mínimo: 21
* SDK de compilación: 30
* JDK 1.8

### Descripción técnica para el funcionamiento del deep link ###

Se creó un deep link el cual direcciona al usuario a la vista del detalle de la película. Consideraciones:

* El deep link depe enviar el parámetro movieId para la posterior consulta de información.
* De no recibir un parámetro válido, la app se re direcciona a la vista de la lista.
* El link se puede ejecutar desde un mensaje copiado en la app de WhatsApp o en la app de mensajes de texto.
* El link de prueba es jeferson.testmerqueo.com/movie_detail/399566
* Este link se debe enviar o copiar como un mensaje en las app mencionadas y desde ahí se podrá validar su funcionamiento.

### Descripción de la responsabilidad de cada capa ###

El proyecto se basa en Clean Architecture usando el patrón de presentación MVVM, y se trabajo con las siguientes capas:

* Presentation (app): En esta capa se integra todo lo que tiene que ver con la interfaz de usuario (Activities/Fragments) y sus respectivos view models.
* Use Cases: Se identifican las acciones más relevantes que el usuario puede ejecutar en el uso de la app y se migran en esta capa, ejemplo (Consultar las películas de la base de datos local).
* Domain: Se agregan las entidades con las que se trabaja el modelo de negocio de la app.
* Data: En esta capa se hace uso del patrón repository y se establecen las distintas fuentes de datos a utilizar a partir de abstracciones.
* Framework: En esta capa se realiza el detalle de la implementación del uso de bibliotecas como retrofit, room, glide.

### Screenshot Lista de películas ###

![Getting Started](./images/movie_list.jpeg)

### Screenshot Detalle de la película ###

![Getting Started](./images/detail_movie.jpeg)

### Screenshots carrito de compras ###

![Getting Started](./images/cart_list.jpeg)

![Getting Started](./images/bottom_sheet.jpeg)

![Getting Started](./images/empty_cart.jpeg)