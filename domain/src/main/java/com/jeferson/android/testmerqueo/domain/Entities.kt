package com.jeferson.android.testmerqueo.domain

data class ResultDomain(
    val adult: Boolean,
    val backdrop_path: String?,
    val genre_ids: List<Int>?,
    val id: Int,
    val original_language: String,
    val original_title: String,
    val overview: String?,
    val popularity: Double,
    val poster_path: String?,
    val release_date: String?,
    val title: String,
    val video: Boolean?,
    val vote_average: Float?,
    val vote_count: Int?,
    var isAddedToShoppingCart: Boolean = false
)

data class BackdropDomain(
    val aspect_ratio: Double,
    val file_path: String?,
    val height: Int,
    val iso_639_1: String?,
    val vote_average: Double?,
    val vote_count: Int?,
    val width: Int
)

data class AuthorDetailsDomain(
    val avatar_path: String?,
    val name: String,
    val rating: Double?,
    val username: String
)

data class ReviewResultDomain(
    val author: String,
    val author_details: AuthorDetailsDomain,
    val content: String,
    val created_at: String?,
    val id: String,
    val updated_at: String?,
    val url: String?
)

data class ShoppingCartMovieDomain(
    val backdrop_path: String?,
    val id: Int,
    val overview: String?,
    val poster_path: String?,
    val release_date: String?,
    val title: String
)