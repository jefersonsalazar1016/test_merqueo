package com.jeferson.android.testmerqueo.data.di

import com.jeferson.android.testmerqueo.data.*
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    fun popularMovieRepositoryProvider(
        remotePopularMovieDataSource: RemotePopularMovieDataSource,
        localPopularMovieDataSource: LocalPopularMovieDataSource
    ) = PopularMovieRepository(remotePopularMovieDataSource, localPopularMovieDataSource)

    @Provides
    fun imagesByMovieIdRepositoryProvider(
        remoteImagesByMovieIdDataSource: RemoteImagesByMovieIdDataSource
    ) = ImagesByMovieIdRepository(remoteImagesByMovieIdDataSource)

    @Provides
    fun reviewsMovieRepositoryProvider(
        remoteReviewsMovieDataSource: RemoteReviewsMovieDataSource
    ) = ReviewsMovieRepository(remoteReviewsMovieDataSource)

    @Provides
    fun shoppingCartRepositoryProvider(
        localShoppingCartMoviesDataSource: LocalShoppingCartMoviesDataSource
    ) = ShoppingCartRepository(localShoppingCartMoviesDataSource)
}