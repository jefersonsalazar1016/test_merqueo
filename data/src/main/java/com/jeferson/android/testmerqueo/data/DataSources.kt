package com.jeferson.android.testmerqueo.data

import com.jeferson.android.testmerqueo.domain.BackdropDomain
import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.domain.ReviewResultDomain
import com.jeferson.android.testmerqueo.domain.ShoppingCartMovieDomain
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single

interface RemotePopularMovieDataSource {
    fun getPopularMovies(page: Int): Single<List<ResultDomain>>

    fun getMovieById(movieId: Int): Single<ResultDomain>
}

interface RemoteImagesByMovieIdDataSource {
    fun getImagesByMovieId(movieId: Int): Single<List<BackdropDomain>>
}

interface LocalPopularMovieDataSource {
    fun getMoviesLocalDB(): Flowable<List<ResultDomain>>

    fun addAllPopularMovies(movieList: List<ResultDomain>): Completable
}

interface RemoteReviewsMovieDataSource {
    fun getAllReviewsMovie(movieId: Int): Single<List<ReviewResultDomain>>
}

interface LocalShoppingCartMoviesDataSource {
    fun getAllShoppingCartMovies(): Flowable<List<ShoppingCartMovieDomain>>

    fun getShoppingCartMovieStatus(movieId: Int): Maybe<Boolean>

    fun updateShoppingCartMovieStatus(shoppingCartMovieDomain: ShoppingCartMovieDomain): Maybe<Boolean>

    fun deleteAllShoppingCartMovies(): Completable
}