package com.jeferson.android.testmerqueo.data

import com.jeferson.android.testmerqueo.domain.BackdropDomain
import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.domain.ReviewResultDomain
import com.jeferson.android.testmerqueo.domain.ShoppingCartMovieDomain
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single

class PopularMovieRepository(
    private val remotePopularMovieDataSource: RemotePopularMovieDataSource,
    private val localPopularMovieDataSource: LocalPopularMovieDataSource
) {
    fun getPopularMovies(page: Int): Single<List<ResultDomain>> =
        remotePopularMovieDataSource.getPopularMovies(page)

    fun getMoviesLocalDB(): Flowable<List<ResultDomain>> =
        localPopularMovieDataSource.getMoviesLocalDB()

    fun addAllPopularMovies(movieList: List<ResultDomain>): Completable =
        localPopularMovieDataSource.addAllPopularMovies(movieList)

    fun getMovieById(movieId: Int): Single<ResultDomain> =
        remotePopularMovieDataSource.getMovieById(movieId)
}

class ImagesByMovieIdRepository(
    private val remoteImagesByMovieIdDataSource: RemoteImagesByMovieIdDataSource
) {
    fun getImagesByMovieId(movieId: Int): Single<List<BackdropDomain>> =
        remoteImagesByMovieIdDataSource.getImagesByMovieId(movieId)
}

class ReviewsMovieRepository(
    private val remoteReviewsMovieDataSource: RemoteReviewsMovieDataSource
) {
    fun getAllReviewsMovie(movieId: Int): Single<List<ReviewResultDomain>> =
        remoteReviewsMovieDataSource.getAllReviewsMovie(movieId)
}

class ShoppingCartRepository(
    private val localShoppingCartMoviesDataSource: LocalShoppingCartMoviesDataSource
) {
    fun getAllShoppingCartMovies(): Flowable<List<ShoppingCartMovieDomain>> =
        localShoppingCartMoviesDataSource.getAllShoppingCartMovies()

    fun getShoppingCartMovieStatus(movieId: Int): Maybe<Boolean> =
        localShoppingCartMoviesDataSource.getShoppingCartMovieStatus(movieId)

    fun updateShoppingCartMovieStatus(shoppingCartMovieDomain: ShoppingCartMovieDomain): Maybe<Boolean> =
        localShoppingCartMoviesDataSource.updateShoppingCartMovieStatus(shoppingCartMovieDomain)

    fun deleteAllShoppingCartMovies(): Completable =
        localShoppingCartMoviesDataSource.deleteAllShoppingCartMovies()
}