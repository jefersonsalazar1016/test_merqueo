package com.jeferson.android.testmerqueo.usecases.di

import com.jeferson.android.testmerqueo.data.ImagesByMovieIdRepository
import com.jeferson.android.testmerqueo.data.PopularMovieRepository
import com.jeferson.android.testmerqueo.data.ReviewsMovieRepository
import com.jeferson.android.testmerqueo.data.ShoppingCartRepository
import com.jeferson.android.testmerqueo.usecases.*
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {

    @Provides
    fun getPopularMoviesUseCaseProvider(
        popularMovieRepository: PopularMovieRepository
    ) = GetPopularMoviesUseCase(popularMovieRepository)

    @Provides
    fun getImagesForMovieIdUseCaseProvider(
        imagesByMovieIdRepository: ImagesByMovieIdRepository
    ) = GetImagesForMovieIdUseCase(imagesByMovieIdRepository)

    @Provides
    fun getMoviesLocalDBUseCaseProvider(
        popularMovieRepository: PopularMovieRepository
    ) = GetMoviesLocalDBUseCase(popularMovieRepository)

    @Provides
    fun getAddAllPopularMoviesUseCaseProvider(
        popularMovieRepository: PopularMovieRepository
    ) = AddAllPopularMoviesUseCase(popularMovieRepository)

    @Provides
    fun getReviewsMovieUseCaseProvider(
        reviewsMovieRepository: ReviewsMovieRepository
    ) = GetReviewsMovieUseCase(reviewsMovieRepository)

    // provides shopping cart use cases
    @Provides
    fun getAllShoppingCartMoviesUseCaseProvider(
        shoppingCartRepository: ShoppingCartRepository
    ) = GetAllShoppingCartMoviesUseCase(shoppingCartRepository)

    @Provides
    fun getShoppingCartMovieStatusUseCaseProvider(
        shoppingCartRepository: ShoppingCartRepository
    ) = GetShoppingCartMovieStatusUseCase(shoppingCartRepository)

    @Provides
    fun updateShoppingCartMovieStatusUseCaseProvider(
        shoppingCartRepository: ShoppingCartRepository
    ) = UpdateShoppingCartMovieStatusUseCase(shoppingCartRepository)

    @Provides
    fun deleteAllShoppingCartMoviesUseCaseProvider(
        shoppingCartRepository: ShoppingCartRepository
    ) = DeleteAllShoppingCartMoviesUseCase(shoppingCartRepository)

    @Provides
    fun getMovieByIdUseCaseProvider(
        popularMovieRepository: PopularMovieRepository
    ) = GetMovieByIdUseCase(popularMovieRepository)
}