package com.jeferson.android.testmerqueo.usecases

import com.jeferson.android.testmerqueo.data.ImagesByMovieIdRepository
import com.jeferson.android.testmerqueo.data.PopularMovieRepository
import com.jeferson.android.testmerqueo.data.ReviewsMovieRepository
import com.jeferson.android.testmerqueo.data.ShoppingCartRepository
import com.jeferson.android.testmerqueo.domain.BackdropDomain
import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.domain.ReviewResultDomain
import com.jeferson.android.testmerqueo.domain.ShoppingCartMovieDomain
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single

class GetPopularMoviesUseCase(
    private val popularMovieRepository: PopularMovieRepository
) {
    fun invoke(currentPage: Int): Single<List<ResultDomain>> =
        popularMovieRepository.getPopularMovies(currentPage)
}

class GetImagesForMovieIdUseCase(
    private val imagesByMovieIdRepository: ImagesByMovieIdRepository
) {
    fun invoke(movieId: Int): Single<List<BackdropDomain>> =
        imagesByMovieIdRepository.getImagesByMovieId(movieId)
}

class GetMoviesLocalDBUseCase(
    private val popularMovieRepository: PopularMovieRepository
) {
    fun invoke(): Flowable<List<ResultDomain>> = popularMovieRepository.getMoviesLocalDB()
}

class AddAllPopularMoviesUseCase(
    private val popularMovieRepository: PopularMovieRepository
) {
    fun invoke(movieList: List<ResultDomain>): Completable =
        popularMovieRepository.addAllPopularMovies(movieList)
}

// reviews use cases
class GetReviewsMovieUseCase(
    private val reviewsMovieRepository: ReviewsMovieRepository
) {
    fun invoke(movieId: Int): Single<List<ReviewResultDomain>> =
        reviewsMovieRepository.getAllReviewsMovie(movieId)
}


// shopping cart use cases
class GetAllShoppingCartMoviesUseCase(
    private val shoppingCartRepository: ShoppingCartRepository
) {
    fun invoke(): Flowable<List<ShoppingCartMovieDomain>> =
        shoppingCartRepository.getAllShoppingCartMovies()
}

class GetShoppingCartMovieStatusUseCase(
    private val shoppingCartRepository: ShoppingCartRepository
) {
    fun invoke(movieId: Int): Maybe<Boolean> =
        shoppingCartRepository.getShoppingCartMovieStatus(movieId)
}

class UpdateShoppingCartMovieStatusUseCase(
    private val shoppingCartRepository: ShoppingCartRepository
) {
    fun invoke(shoppingCartMovieDomain: ShoppingCartMovieDomain): Maybe<Boolean> =
        shoppingCartRepository.updateShoppingCartMovieStatus(shoppingCartMovieDomain)
}

class DeleteAllShoppingCartMoviesUseCase(
    private val shoppingCartRepository: ShoppingCartRepository
) {
    fun invoke(): Completable = shoppingCartRepository.deleteAllShoppingCartMovies()
}

class GetMovieByIdUseCase(
    private val popularMovieRepository: PopularMovieRepository
) {
    fun invoke(movieId: Int): Single<ResultDomain> = popularMovieRepository.getMovieById(movieId)
}