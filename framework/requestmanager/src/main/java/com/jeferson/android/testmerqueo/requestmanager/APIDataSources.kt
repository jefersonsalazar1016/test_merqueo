package com.jeferson.android.testmerqueo.requestmanager

import com.jeferson.android.testmerqueo.data.RemoteImagesByMovieIdDataSource
import com.jeferson.android.testmerqueo.data.RemotePopularMovieDataSource
import com.jeferson.android.testmerqueo.data.RemoteReviewsMovieDataSource
import com.jeferson.android.testmerqueo.domain.BackdropDomain
import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.domain.ReviewResultDomain
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PopularMovieRetrofitDataSource(
    private val movieRequest: PopularMovieRequest
) : RemotePopularMovieDataSource {
    override fun getPopularMovies(page: Int): Single<List<ResultDomain>> {
        return movieRequest.getService<PopularMovieService>()
            .getPopularMovies(page = page)
            .map(PopularMovieResponseServer::toResultDomainList)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

    override fun getMovieById(movieId: Int): Single<ResultDomain> {
        return movieRequest.getService<PopularMovieService>()
            .getMovieById(movieId)
            .map(DetailMovieResponseServer::toResultDomain)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

}

class ImagesByMovieIdRetrofitDataSource(
    private val imagesMovieRequest: ImagesMovieRequest
) : RemoteImagesByMovieIdDataSource {
    override fun getImagesByMovieId(movieId: Int): Single<List<BackdropDomain>> {
        return imagesMovieRequest.getService<ImagesMovieService>()
            .getImagesForIdMovie(movie_id = movieId)
            .map(ImagesMovieResponseServer::toBackdropsDomainList)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

}

class ReviewsMovieRetrofitDataSource(
    private val reviewsMovieRequest: ReviewsMovieRequest
) : RemoteReviewsMovieDataSource {
    override fun getAllReviewsMovie(movieId: Int): Single<List<ReviewResultDomain>> {
        return reviewsMovieRequest.getService<ReviewsMovieService>()
            .getReviewsForIdMovie(movieId)
            .map(ReviewsMovieResponseServer::toReviewsDomainList)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }
}

