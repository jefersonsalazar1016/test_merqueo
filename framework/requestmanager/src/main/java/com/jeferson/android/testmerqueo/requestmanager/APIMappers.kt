package com.jeferson.android.testmerqueo.requestmanager

import com.jeferson.android.testmerqueo.domain.AuthorDetailsDomain
import com.jeferson.android.testmerqueo.domain.BackdropDomain
import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.domain.ReviewResultDomain

fun PopularMovieResponseServer.toResultDomainList(): List<ResultDomain> = results.map {
    it.run {
        ResultDomain(
            adult,
            backdrop_path,
            genre_ids,
            id,
            original_language,
            original_title,
            overview,
            popularity,
            poster_path,
            release_date,
            title,
            video,
            vote_average,
            vote_count
        )
    }
}

fun ImagesMovieResponseServer.toBackdropsDomainList(): List<BackdropDomain> = backdrops.map {
    it.run {
        BackdropDomain(
            aspect_ratio,
            file_path,
            height,
            iso_639_1,
            vote_average,
            vote_count,
            width
        )
    }
}

fun ReviewsMovieResponseServer.toReviewsDomainList(): List<ReviewResultDomain> = results.map {
    it.run {
        ReviewResultDomain(
            author,
            author_details.toAuthorDetailsDomain(),
            content,
            created_at,
            id,
            updated_at,
            url
        )
    }
}

fun AuthorDetailsServer.toAuthorDetailsDomain() = AuthorDetailsDomain(
    avatar_path,
    name,
    rating,
    username
)

fun DetailMovieResponseServer.toResultDomain() = ResultDomain(
    adult,
    backdrop_path,
    null,
    id,
    original_language,
    original_title,
    overview,
    popularity,
    poster_path,
    release_date,
    title,
    video,
    vote_average,
    vote_count
)