package com.jeferson.android.testmerqueo.requestmanager

import com.jeferson.android.testmerqueo.requestmanager.APIConstants.API_KEY
import com.jeferson.android.testmerqueo.requestmanager.APIConstants.API_KEY_VALUE
import com.jeferson.android.testmerqueo.requestmanager.APIConstants.END_POINT_IMAGES_MOVIE
import com.jeferson.android.testmerqueo.requestmanager.APIConstants.END_POINT_POPULAR_MOVIES
import com.jeferson.android.testmerqueo.requestmanager.APIConstants.END_POINT_POPULAR_MOVIE_BY_ID
import com.jeferson.android.testmerqueo.requestmanager.APIConstants.END_POINT_REVIEWS_MOVIE
import com.jeferson.android.testmerqueo.requestmanager.APIConstants.LANGUAGE_KEY
import com.jeferson.android.testmerqueo.requestmanager.APIConstants.LANGUAGE_VALUE_ES
import com.jeferson.android.testmerqueo.requestmanager.APIConstants.PAGE_KEY
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PopularMovieService {

    @GET(END_POINT_POPULAR_MOVIES)
    fun getPopularMovies(
        @Query(API_KEY) apiKey: String = API_KEY_VALUE,
        @Query(PAGE_KEY) page: Int,
        @Query(LANGUAGE_KEY) language: String = LANGUAGE_VALUE_ES
    ): Single<PopularMovieResponseServer>

    @GET(END_POINT_POPULAR_MOVIE_BY_ID)
    fun getMovieById(
        @Path("movie_id") movie_id: Int,
        @Query(API_KEY) apiKey: String = API_KEY_VALUE,
        @Query(LANGUAGE_KEY) language: String = LANGUAGE_VALUE_ES
    ): Single<DetailMovieResponseServer>
}

interface ImagesMovieService {

    @GET(END_POINT_IMAGES_MOVIE)
    fun getImagesForIdMovie(
        @Path("movie_id") movie_id: Int,
        @Query(API_KEY) apiKey: String = API_KEY_VALUE,
    ): Single<ImagesMovieResponseServer>
}

interface ReviewsMovieService {

    @GET(END_POINT_REVIEWS_MOVIE)
    fun getReviewsForIdMovie(
        @Path("movie_id") movie_id: Int,
        @Query(API_KEY) apiKey: String = API_KEY_VALUE,
    ): Single<ReviewsMovieResponseServer>
}