package com.jeferson.android.testmerqueo.requestmanager

data class PopularMovieResponseServer(
    val page: Int,
    val results: List<ResultServer>,
    val total_pages: Int,
    val total_results: Int
)

data class ResultServer(
    val adult: Boolean,
    val backdrop_path: String?,
    val genre_ids: List<Int>?,
    val id: Int,
    val original_language: String,
    val original_title: String,
    val overview: String?,
    val popularity: Double,
    val poster_path: String?,
    val release_date: String?,
    val title: String,
    val video: Boolean,
    val vote_average: Float,
    val vote_count: Int
)


// Images classes

data class ImagesMovieResponseServer(
    val backdrops: List<BackdropServer>,
    val id: Int,
    val posters: List<PosterServer>
)

data class BackdropServer(
    val aspect_ratio: Double,
    val file_path: String?,
    val height: Int,
    val iso_639_1: String?,
    val vote_average: Double?,
    val vote_count: Int?,
    val width: Int
)

data class PosterServer(
    val aspect_ratio: Double,
    val file_path: String?,
    val height: Int,
    val iso_639_1: String?,
    val vote_average: Double?,
    val vote_count: Int?,
    val width: Int
)

// reviews response

data class ReviewsMovieResponseServer(
    val id: Int,
    val page: Int,
    val results: List<ReviewResultServer>,
    val total_pages: Int,
    val total_results: Int
)

data class AuthorDetailsServer(
    val avatar_path: String?,
    val name: String,
    val rating: Double?,
    val username: String
)

data class ReviewResultServer(
    val author: String,
    val author_details: AuthorDetailsServer,
    val content: String,
    val created_at: String?,
    val id: String,
    val updated_at: String?,
    val url: String?
)

// movie by id response

data class DetailMovieResponseServer(
    val adult: Boolean,
    val backdrop_path: String?,
    val belongs_to_collection: Any?,
    val budget: Int?,
    val genres: List<GenreServer>?,
    val homepage: String,
    val id: Int,
    val imdb_id: String,
    val original_language: String,
    val original_title: String,
    val overview: String,
    val popularity: Double,
    val poster_path: String?,
    val production_companies: List<ProductionCompanyServer>?,
    val production_countries: List<ProductionCountryServer>?,
    val release_date: String?,
    val revenue: Int?,
    val runtime: Int?,
    val spoken_languages: List<SpokenLanguageServer>?,
    val status: String?,
    val tagline: String?,
    val title: String,
    val video: Boolean?,
    val vote_average: Float?,
    val vote_count: Int?
)

data class GenreServer(
    val id: Int,
    val name: String?
)

data class ProductionCompanyServer(
    val id: Int,
    val logo_path: String?,
    val name: String?,
    val origin_country: String?
)

data class ProductionCountryServer(
    val iso_3166_1: String?,
    val name: String?
)

data class SpokenLanguageServer(
    val english_name: String?,
    val iso_639_1: String?,
    val name: String?
)