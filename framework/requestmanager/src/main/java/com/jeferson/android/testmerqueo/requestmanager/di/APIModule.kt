package com.jeferson.android.testmerqueo.requestmanager.di

import com.jeferson.android.testmerqueo.data.RemoteImagesByMovieIdDataSource
import com.jeferson.android.testmerqueo.data.RemotePopularMovieDataSource
import com.jeferson.android.testmerqueo.data.RemoteReviewsMovieDataSource
import com.jeferson.android.testmerqueo.requestmanager.*
import com.jeferson.android.testmerqueo.requestmanager.APIConstants.BASE_API_URL
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class APIModule {

    @Provides
    fun popularMovieRequestProvider(
        @Named("baseUrl") baseUrl: String
    ) = PopularMovieRequest(baseUrl)

    @Provides
    @Singleton
    @Named("baseUrl")
    fun baseUrlProvider(): String = BASE_API_URL

    @Provides
    fun remotePopularMovieDataSourceProvider(
        popularMovieRequest: PopularMovieRequest
    ): RemotePopularMovieDataSource = PopularMovieRetrofitDataSource(popularMovieRequest)

    @Provides
    fun imagesMovieRequestProvider(
        @Named("baseUrl") baseUrl: String
    ) = ImagesMovieRequest(baseUrl)

    @Provides
    fun remoteImagesByMovieIdDataSourceProvider(
        imagesMovieRequest: ImagesMovieRequest
    ): RemoteImagesByMovieIdDataSource = ImagesByMovieIdRetrofitDataSource(imagesMovieRequest)

    @Provides
    fun reviewsMovieRequestProvider(
        @Named("baseUrl") baseUrl: String
    ) = ReviewsMovieRequest(baseUrl)

    @Provides
    fun remoteReviewsMovieDataSourceProvider(
        reviewsMovieRequest: ReviewsMovieRequest
    ): RemoteReviewsMovieDataSource = ReviewsMovieRetrofitDataSource(reviewsMovieRequest)
}