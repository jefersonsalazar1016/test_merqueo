package com.jeferson.android.testmerqueo.databasemanager.utils

object DBConstants {
    const val DATABASE_NAME = "test_merqueo_db"
    const val DB_VERSION = 2

    // tables constants
    const val TABLE_MOVIE = "movie"
    const val TABLE_SHOPPING_CART_MOVIE = "cart_movie"

    // movie properties
    const val ADULT = "adult"
    const val BACKDROP_PATH = "backdrop_path"
    const val ID_RESULT = "id"
    const val ORIGINAL_LANGUAGE = "original_language"
    const val ORIGINAL_TITLE = "original_title"
    const val OVERVIEW = "overview"
    const val POPULARITY = "popularity"
    const val POSTER_PATH = "poster_path"
    const val RELEASE_DATE = "release_date"
    const val TITLE_RESULT = "title"
    const val VIDEO_RESULT = "video"
    const val VOTE_AVERAGE = "vote_average"
    const val VOTE_COUNT = "vote_count"
}