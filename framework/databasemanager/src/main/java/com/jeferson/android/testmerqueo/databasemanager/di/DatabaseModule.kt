package com.jeferson.android.testmerqueo.databasemanager.di

import android.app.Application
import com.jeferson.android.testmerqueo.data.LocalPopularMovieDataSource
import com.jeferson.android.testmerqueo.data.LocalShoppingCartMoviesDataSource
import com.jeferson.android.testmerqueo.databasemanager.PopularMovieRoomDataSource
import com.jeferson.android.testmerqueo.databasemanager.ShoppingCartMovieRoomDataSource
import com.jeferson.android.testmerqueo.databasemanager.TestMerqueoDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun databaseProvider(app: Application): TestMerqueoDatabase =
        TestMerqueoDatabase.getDatabase(app)

    @Provides
    fun localPopularMovieDataSourceProvider(
        database: TestMerqueoDatabase
    ): LocalPopularMovieDataSource = PopularMovieRoomDataSource(database)

    @Provides
    fun localShoppingCartMoviesDataSourceProvider(
        database: TestMerqueoDatabase
    ): LocalShoppingCartMoviesDataSource = ShoppingCartMovieRoomDataSource(database)
}