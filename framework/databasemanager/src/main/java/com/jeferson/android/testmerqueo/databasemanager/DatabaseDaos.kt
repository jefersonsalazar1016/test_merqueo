package com.jeferson.android.testmerqueo.databasemanager

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe

@Dao
interface PopularMovieDao {

    @Query("SELECT * FROM movie")
    fun getAllMoviesLocalDB(): Flowable<List<ResultEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllListPopularMovies(movieList: List<ResultEntity>): Completable
}

@Dao
interface ShoppingCartMovieDao {

    @Query("SELECT * FROM cart_movie")
    fun getAllShoppingCartMovies(): Flowable<List<ShoppingCartMovieEntity>>

    @Query("SELECT * FROM cart_movie WHERE id = :id")
    fun getShoppingCartMovieById(id: Int): Maybe<ShoppingCartMovieEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertShoppingCartMovie(shoppingCartMovieEntity: ShoppingCartMovieEntity)

    @Delete
    fun deleteShoppingCartMovie(shoppingCartMovieEntity: ShoppingCartMovieEntity)

    @Query("DELETE FROM cart_movie")
    fun deleteAllShoppingCartMovies(): Completable
}