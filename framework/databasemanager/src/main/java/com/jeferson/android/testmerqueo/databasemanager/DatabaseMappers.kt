package com.jeferson.android.testmerqueo.databasemanager

import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.domain.ShoppingCartMovieDomain

fun List<ResultEntity>.toResultDomainList() = map(ResultEntity::toResultDomain)

fun List<ResultDomain>.toResultEntityList() = map(ResultDomain::toResultEntity)

fun ResultEntity.toResultDomain() = ResultDomain(
    adult,
    backdrop_path,
    null,
    id,
    original_language ?: "",
    original_title ?: "",
    overview,
    popularity,
    poster_path,
    release_date,
    title ?: "",
    video,
    vote_average,
    vote_count
)

fun ResultDomain.toResultEntity() = ResultEntity(
    adult,
    backdrop_path,
    id,
    original_language,
    original_title,
    overview,
    popularity,
    poster_path,
    release_date,
    title,
    video,
    vote_average,
    vote_count
)

// mappers for shopping cart
fun List<ShoppingCartMovieEntity>.toShoppingCartMovieDomain() =
    map(ShoppingCartMovieEntity::toShoppingCartMovieDomain)

fun ShoppingCartMovieEntity.toShoppingCartMovieDomain() = ShoppingCartMovieDomain(
    backdrop_path,
    id,
    overview,
    poster_path,
    release_date,
    title
)

fun ShoppingCartMovieDomain.toShoppingCartMovieEntity() = ShoppingCartMovieEntity(
    backdrop_path,
    id,
    overview,
    poster_path,
    release_date,
    title
)