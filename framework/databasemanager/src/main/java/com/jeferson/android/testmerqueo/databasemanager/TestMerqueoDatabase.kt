package com.jeferson.android.testmerqueo.databasemanager

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.DATABASE_NAME
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.DB_VERSION

@Database(
    entities = [ResultEntity::class, ShoppingCartMovieEntity::class],
    version = DB_VERSION,
    exportSchema = false
)
abstract class TestMerqueoDatabase : RoomDatabase() {

    abstract fun popularMovieDao(): PopularMovieDao
    abstract fun shoppingCartMovieDao(): ShoppingCartMovieDao

    companion object {
        // migrations
        private val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL(
                    "CREATE TABLE cart_movie (backdrop_path TEXT, id INTEGER NOT NULL, overview TEXT, poster_path TEXT, release_date TEXT, title TEXT NOT NULL, PRIMARY KEY(id))"
                )
            }
        }

        @Synchronized
        fun getDatabase(context: Context): TestMerqueoDatabase = Room.databaseBuilder(
            context.applicationContext,
            TestMerqueoDatabase::class.java,
            DATABASE_NAME
        ).addMigrations(MIGRATION_1_2).build()
    }
}