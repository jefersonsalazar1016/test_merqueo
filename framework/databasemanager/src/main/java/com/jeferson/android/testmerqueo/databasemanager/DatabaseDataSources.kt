package com.jeferson.android.testmerqueo.databasemanager

import com.jeferson.android.testmerqueo.data.LocalPopularMovieDataSource
import com.jeferson.android.testmerqueo.data.LocalShoppingCartMoviesDataSource
import com.jeferson.android.testmerqueo.domain.ResultDomain
import com.jeferson.android.testmerqueo.domain.ShoppingCartMovieDomain
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PopularMovieRoomDataSource(
    database: TestMerqueoDatabase
) : LocalPopularMovieDataSource {

    // dao instance
    private val popularMovieDao by lazy { database.popularMovieDao() }

    override fun getMoviesLocalDB(): Flowable<List<ResultDomain>> = popularMovieDao
        .getAllMoviesLocalDB()
        .map(List<ResultEntity>::toResultDomainList)
        .onErrorReturn { emptyList() }
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())

    override fun addAllPopularMovies(movieList: List<ResultDomain>): Completable = popularMovieDao
        .insertAllListPopularMovies(movieList::toResultEntityList.invoke())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
}

class ShoppingCartMovieRoomDataSource(
    database: TestMerqueoDatabase
) : LocalShoppingCartMoviesDataSource {

    // dao instance
    private val shoppingCartMovieDao by lazy { database.shoppingCartMovieDao() }

    override fun getAllShoppingCartMovies(): Flowable<List<ShoppingCartMovieDomain>> {
        return shoppingCartMovieDao
            .getAllShoppingCartMovies()
            .map(List<ShoppingCartMovieEntity>::toShoppingCartMovieDomain)
            .onErrorReturn { emptyList() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

    override fun getShoppingCartMovieStatus(movieId: Int): Maybe<Boolean> {
        return shoppingCartMovieDao
            .getShoppingCartMovieById(movieId)
            .isEmpty
            .flatMapMaybe { isEmpty ->
                Maybe.just(!isEmpty)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

    override fun updateShoppingCartMovieStatus(shoppingCartMovieDomain: ShoppingCartMovieDomain): Maybe<Boolean> {
        val shoppingCartMovieEntity = shoppingCartMovieDomain.toShoppingCartMovieEntity()
        return shoppingCartMovieDao
            .getShoppingCartMovieById(shoppingCartMovieEntity.id)
            .isEmpty
            .flatMapMaybe { isEmpty ->
                if (isEmpty) {
                    shoppingCartMovieDao.insertShoppingCartMovie(shoppingCartMovieEntity)
                } else {
                    shoppingCartMovieDao.deleteShoppingCartMovie(shoppingCartMovieEntity)
                }
                Maybe.just(isEmpty)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

    override fun deleteAllShoppingCartMovies(): Completable {
        return shoppingCartMovieDao
            .deleteAllShoppingCartMovies()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

}