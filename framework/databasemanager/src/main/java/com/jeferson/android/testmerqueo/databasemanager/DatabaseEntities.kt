package com.jeferson.android.testmerqueo.databasemanager

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.ADULT
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.BACKDROP_PATH
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.ID_RESULT
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.ORIGINAL_LANGUAGE
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.ORIGINAL_TITLE
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.OVERVIEW
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.POPULARITY
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.POSTER_PATH
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.RELEASE_DATE
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.TABLE_MOVIE
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.TABLE_SHOPPING_CART_MOVIE
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.TITLE_RESULT
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.VIDEO_RESULT
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.VOTE_AVERAGE
import com.jeferson.android.testmerqueo.databasemanager.utils.DBConstants.VOTE_COUNT

@Entity(tableName = TABLE_MOVIE)
data class ResultEntity(
    @ColumnInfo(name = ADULT) var adult: Boolean,
    @ColumnInfo(name = BACKDROP_PATH) var backdrop_path: String?,
    @PrimaryKey @ColumnInfo(name = ID_RESULT) var id: Int,
    @ColumnInfo(name = ORIGINAL_LANGUAGE) var original_language: String?,
    @ColumnInfo(name = ORIGINAL_TITLE) var original_title: String?,
    @ColumnInfo(name = OVERVIEW) var overview: String?,
    @ColumnInfo(name = POPULARITY) var popularity: Double = 0.0,
    @ColumnInfo(name = POSTER_PATH) var poster_path: String?,
    @ColumnInfo(name = RELEASE_DATE) var release_date: String?,
    @ColumnInfo(name = TITLE_RESULT) var title: String?,
    @ColumnInfo(name = VIDEO_RESULT) var video: Boolean?,
    @ColumnInfo(name = VOTE_AVERAGE) var vote_average: Float?,
    @ColumnInfo(name = VOTE_COUNT) var vote_count: Int?
)

@Entity(tableName = TABLE_SHOPPING_CART_MOVIE)
data class ShoppingCartMovieEntity(
    @ColumnInfo(name = BACKDROP_PATH) var backdrop_path: String?,
    @PrimaryKey @ColumnInfo(name = ID_RESULT) var id: Int,
    @ColumnInfo(name = OVERVIEW) var overview: String?,
    @ColumnInfo(name = POSTER_PATH) var poster_path: String?,
    @ColumnInfo(name = RELEASE_DATE) var release_date: String?,
    @ColumnInfo(name = TITLE_RESULT) var title: String,
)